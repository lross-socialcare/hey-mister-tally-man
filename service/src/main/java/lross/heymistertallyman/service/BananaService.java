package lross.heymistertallyman.service;

import lross.heymistertallyman.api.BananaApi;
import lross.heymistertallyman.domain.Banana;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@Component
public class BananaService implements BananaApi {

    @Override
    public Collection<Banana> getRipeBananas() {

        return Arrays.asList(new Banana(1.0d), new Banana(Double.MAX_VALUE));
    }
}

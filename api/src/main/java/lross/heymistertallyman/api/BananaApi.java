package lross.heymistertallyman.api;

import lross.heymistertallyman.domain.Banana;

import java.util.Collection;

public interface BananaApi {

    /**
     * Gets all bananas where {@link Banana#getRipeness()} is greater than zero.
     *
     * @return  some bananas
     */
    Collection<Banana> getRipeBananas();
}

package lross.heymistertallyman.domain;

import lombok.Data;

@Data
public class Banana {

    private final double ripeness;
}

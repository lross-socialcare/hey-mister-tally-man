package lross.heymistertallyman.web;

import lombok.RequiredArgsConstructor;
import lross.heymistertallyman.domain.Banana;
import lross.heymistertallyman.service.BananaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
public class BananaController {

    private final BananaService bananaService;

    @RequestMapping("/")
    public Collection<Banana> getRipeBananas() {

        return bananaService.getRipeBananas();
    }
}

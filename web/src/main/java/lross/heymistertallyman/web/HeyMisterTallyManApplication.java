package lross.heymistertallyman.web;

import lross.heymistertallyman.service.HeyMisterTallyManServiceSpringConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({HeyMisterTallyManServiceSpringConfiguration.class})
public class HeyMisterTallyManApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeyMisterTallyManApplication.class);
    }
}

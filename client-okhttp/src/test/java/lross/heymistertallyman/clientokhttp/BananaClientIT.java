package lross.heymistertallyman.clientokhttp;

import com.fasterxml.jackson.databind.ObjectMapper;
import lross.heymistertallyman.domain.Banana;
import lross.heymistertallyman.web.HeyMisterTallyManApplication;
import okhttp3.OkHttpClient;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {HeyMisterTallyManApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BananaClientIT {

    private @LocalServerPort int port;
    private BananaClient bananaClient;

    @Before
    public void before() {
        bananaClient = new BananaClient(new OkHttpClient(), new ObjectMapper(), "localhost", port);
    }

    @Test
    public void bananaClientShouldReturnBananas() {

        Collection<Banana> apples = bananaClient.getRipeBananas();
        Assertions.assertThat(apples).containsExactly(new Banana(1.0d), new Banana(Double.MAX_VALUE));
    }
}

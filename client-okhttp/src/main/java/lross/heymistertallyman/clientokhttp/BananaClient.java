package lross.heymistertallyman.clientokhttp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lross.heymistertallyman.api.BananaApi;
import lross.heymistertallyman.domain.Banana;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;

@Component
@RequiredArgsConstructor
public class BananaClient implements BananaApi {

    private final OkHttpClient client;
    private final ObjectMapper mapper;
    private final String hostname;
    private final int port;

    @Override
    public Collection<Banana> getRipeBananas() {

        Request request = new Request.Builder()
                .url(new HttpUrl.Builder()
                        .scheme("http")
                        .host(hostname)
                        .port(port)
                        .build())
                .build();
        try {
            Response response = client.newCall(request).execute();
            return mapper.readValue(response.body().string(), new TypeReference<Collection<Banana>>() {});
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
